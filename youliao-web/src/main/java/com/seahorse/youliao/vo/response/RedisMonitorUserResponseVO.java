package com.seahorse.youliao.vo.response;

import com.seahorse.youliao.security.JwtUserRedis;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @ProjectName: youliao
 * @Package: com.seahorse.youliao.vo.response
 * @ClassName: RedisMonitorUserResponseVO
 * @Description: 用户redis 信息
 * @author:songqiang
 * @Date:2020-12-01 17:21
 **/
@ApiModel
@Getter
@Setter
@ToString
public class RedisMonitorUserResponseVO implements Serializable {


    /**
     * 当前页
     **/
    @ApiModelProperty("当前页")
    private Integer pageNum;
    /**
     * 每页条数
     **/
    @ApiModelProperty("每页的数量")
    private Integer pageSize;
    /**
     * 总记录数
     **/
    @ApiModelProperty("总记录数")
    private Long total;
    /**
     * 结果集
     **/
    @ApiModelProperty("结果集")
    private List<JwtUserRedis> list;
}
