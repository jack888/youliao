package com.seahorse.youliao.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @description: 短信登录
 * @author: Mr.Song
 * @create: 2020-11-23 22:07
 **/
@ApiModel
@Getter
@Setter
@ToString
public class MobileLoginRequestVO {

    /**
     * 手机号码
     */
    @ApiModelProperty("手机号码")
    private String mobile;


    /**
     * 验证码
     */
    @ApiModelProperty("验证码")
    private String code;

}
