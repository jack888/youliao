<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>html2canvas example</title>
    <script type="text/javascript" src="/api/js/html2canvas.js"></script>
    <script type="text/javascript" src="/api/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="/api/js/jspdf.debug.js"></script>
</head>
<script language="javascript" type="text/javascript">

    function takeScreenshot() {
        html2canvas(document.getElementById('view'), {
            onrendered: function (canvas) {
                document.body.appendChild(canvas);
                var url = canvas.toDataURL();//图片地址
                console.log(url)
                downloadFile('测试.png', url);

                var doc = new jsPDF('p', 'px','a3');
                //第一列 左右边距  第二列上下边距  第三列是图片左右拉伸  第四列 图片上下拉伸
                doc.addImage(url, 'PNG', -9, 0,650,1500);
                doc.addPage();
                doc.addImage(url, 'PNG', -9, -900,650,1500);
                doc.save('test.pdf')

                $("#curTpl").attr('src', url);
                $("#curTpl").show();
            },
            // logging: true,
            // { allowTaint: true, useCORS: true }可解决跨域图片不显示问题
            allowTaint: true,
            useCORS: true,
            // { backgroundColor: "transparent" }可解决图片不透明问题
            //width: 300,
            //height: 300
        });
    }

    //下载
    function downloadFile(fileName, content) {
        let aLink = document.createElement('a');
        let blob = this.base64ToBlob(content); //new Blob([content]);

        let evt = document.createEvent("HTMLEvents");
        evt.initEvent("click", true, true);//initEvent 不加后两个参数在FF下会报错  事件类型，是否冒泡，是否阻止浏览器的默认行为
        aLink.download = fileName;
        aLink.href = URL.createObjectURL(blob);

        // aLink.dispatchEvent(evt);
        aLink.click()
    }

    //base64转blob
    function base64ToBlob(code) {
        let parts = code.split(';base64,');
        let contentType = parts[0].split(':')[1];
        let raw = window.atob(parts[1]);
        let rawLength = raw.length;

        let uInt8Array = new Uint8Array(rawLength);

        for (let i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }
        return new Blob([uInt8Array], {type: contentType});
    }


</script>
<body>
<input type="button" value="截图" onclick="takeScreenshot()">
<div id="view" style=" width: 700px; height: 700px;">
    <img src="/api/img/qrCode.png" alt="">
</div>
<div style="width:360px;height:533px;background-color:#333;">
    <img id="curTpl" src="" style="width:80%;height:80%;display:none;margin:0 auto;margin-left:30px;margin-top:20px;">
</div>
</body>

</html>