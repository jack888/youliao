﻿﻿<html xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>receipt</title>
    <script type="text/javascript" src="/api/js/X-doc.js"></script>
</head>
<body style="height:100%; margin:0; overflow:hidden;">
<script id="myxdoc" type="text/xdoc" _format="pdf" style="width:100%;height:100%;">
    <xdoc version="A.3.0">
    <paper margin="0" width="300" height="600"/>
    <body padding="10" fillImg="#@f40">

        <para align="center" lineSpacing="10">
            <text fontName="标宋" fontSize="25" >成都七院(天府新院)采血回执单</text>
        </para>
        <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="300">
            <text  fontSize="8">---------------------------------------------------------------------</text>
        </para>
        <para lineSpacing="0">
            <rect color="" name="x6870" width="160" height="100">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="160" >
                    <rect align="center" color="" name="x5246" width="120" height="100"><img
                            src="http://localhost:8080/barCode.jpg"
                            height="100" width="150"/></rect>
                </para>
            </rect>
            <rect color="" align="center" name="x6871" width="80" height="100">
                <para align="center" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="80">
                    <text fontSize="13"> 司 丽 丽 </text>
                    <text fontSize="13"> 女  35岁 </text>
                </para>
            </rect>
        </para>
        <para lineSpacing="1">
            <table cellpadding="50" cellspacing="0" height="200" width="280" sizeType="autoheight" name="x347"
                   header="1" cols="100,100" rows="10,100,100,100">
                <rect col="1" align="center" row="1"  name="x347">
                    <para align="center" lineSpacing="0">
                        <text fontSize="13">项目名称</text>
                    </para>
                </rect>
                <rect col="2" align="center" row="1"  name="x347">
                    <para align="center" lineSpacing="0">
                        <text fontSize="13">取单时间</text>
                    </para>
                </rect>
                <rect col="1" align="center" row="2"  name="x347">
                    <para lineSpacing="0">
                        <text fontSize="13">空腹检测</text>
                    </para>
                </rect>
                <rect col="2" align="center" row="2"  name="x347">
                    <para lineSpacing="0">
                        <text fontSize="13">请分别空腹到窗口检测抽血等待报告 咨询报告</text>
                    </para>
                </rect>
                <rect col="1" align="center" row="3"  name="x347">
                    <para lineSpacing="0">
                        <text fontSize="13">葡萄糖1h检测</text>
                    </para>
                </rect>
                <rect col="2" align="center" row="3"  name="x347">
                    <para lineSpacing="0">
                        <text fontSize="13">请分别食用葡萄糖75g 1小时后到窗口检测抽血等待报告 咨询报告</text>
                    </para>
                </rect>
                <rect col="1" align="center" row="4"  name="x347">
                    <para lineSpacing="0">
                        <text fontSize="13">葡萄糖2h检测</text>
                    </para>
                </rect>
                <rect col="2" align="center" row="4"  name="x347">
                    <para lineSpacing="0">
                        <text fontSize="13">请分别食用葡萄糖75g 2小时后到窗口检测抽血等待报告 咨询报告</text>
                    </para>
                </rect>
            </table>
        </para>

        <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="300">
            <text fontName="标宋"  fontSize="13">如遇异常情况,请详询检验科室咨询窗口</text>
        </para>
        <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="300">
            <text  fontSize="12">2019/3/9 09:09:09</text>
        </para>
        <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="300">
            <text  fontSize="12">取单地点:自助取单机取单</text>
        </para>

        </body>
    </xdoc>
</script>
</body>
</html>
